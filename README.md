##Prerequisites
* Tomcat
* Spring MVC
* Eclipse / Spring IDE
* Maven

##Build Instructions
1. Create a new Spring MVC project with the package name as `com.teamvirus.src`
2. Copy the `.java` files to `source/src/main/java/com/teamvirus/src/`
3. Copy the `.jsp` files to `source/src/main/webapp/WEB-INF/views/`
4. Run server