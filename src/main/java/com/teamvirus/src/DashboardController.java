package com.teamvirus.src;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DashboardController {

	@RequestMapping("/dashboard")
	public String dashboard(
			@RequestParam(value = "username", required = true) String username,
			Model model) {
		if (username.equals("admin")) {
			model.addAttribute("username", username);
			return "dashboard";
		} else if (username.equals("student1")) {
			model.addAttribute("username", username);
			return "studentdashboard";
		} else {
			return "err404";
		}
	}
}
