package com.teamvirus.src;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {
	@RequestMapping("/login")
	public String login() {
		return "login";
	}

	@RequestMapping("/authenticate")
	public String authenticate(
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "password", required = true) String password) {
		if ((username.equals("admin") && password.equals("admin")))
			return "redirect:dashboard?username=" + username;
		else if ((username.equals("student1") && password.equals("student1"))) {
			return "redirect:dashboard?username=" + username;
		}
		return "wrongpassword";
	}
}