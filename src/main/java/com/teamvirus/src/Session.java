package com.teamvirus.src;

public class Session {
	private String date;
	private String time;
	private int duration;
	private int repeatFrequency;
	private String lecturer;
	private int maxAttendance;
	private boolean compulsory;
	private String venue;
	private String courseName;
	private String sessionS;

	public Session() {
	}

	public Session(String date, String time, int duration, int repeatFrequency,
			String lecturer, int maxAttendance, boolean compulsory,
			String venue, String courseName) {
		this.date = date;
		this.time = time;
		this.duration = duration;
		this.repeatFrequency = repeatFrequency;
		this.lecturer = lecturer;
		this.maxAttendance = maxAttendance;
		this.compulsory = compulsory;
		this.venue = venue;
		this.courseName = courseName;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getLecturer() {
		return lecturer;
	}

	public void setLecturer(String lecturer) {
		this.lecturer = lecturer;
	}

	public int getRepeatFrequency() {
		return repeatFrequency;
	}

	public void setRepeatFrequency(int repeatFrequency) {
		this.repeatFrequency = repeatFrequency;
	}

	public int getMaxAttendance() {
		return maxAttendance;
	}

	public void setMaxAttendance(int maxAttendance) {
		this.maxAttendance = maxAttendance;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public boolean isCompulsory() {
		return compulsory;
	}

	public void setCompulsory(boolean compulsory) {
		this.compulsory = compulsory;
	}

	public String getSessionS() {
		return sessionS;
	}

	public void setSessionS(String session) {
		this.sessionS = session;
	}

}
