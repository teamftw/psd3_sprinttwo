package com.teamvirus.src;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Controller
public class SessionController {

	@RequestMapping(value = "/addsession", method = RequestMethod.GET)
	public ModelAndView formSession() {
		return new ModelAndView("addsession", "command", new Session());
	}

	@RequestMapping(value = "/addsession", method = RequestMethod.POST)
	public String submitForm(@ModelAttribute("SpringWeb") Session session,
			ModelMap model) throws IOException {
		model.addAttribute("date", session.getDate());
		model.addAttribute("duration", session.getDuration());
		model.addAttribute("lecturer", session.getLecturer());
		model.addAttribute("MaxAttendance", session.getMaxAttendance());
		model.addAttribute("repeatFrequency", session.getRepeatFrequency());
		model.addAttribute("isCompulsory", session.isCompulsory());
		model.addAttribute("time", session.getTime());
		model.addAttribute("venue", session.getVenue());
		model.addAttribute("courseName", session.getCourseName());

		File file = new File("s1sessionlist.csv");		

		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		// true = append file
		FileWriter fileWritter = new FileWriter(file, true);
		BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
		bufferWritter.write(session.getDate() + ",");
		bufferWritter.write(session.getDuration() + ",");
		bufferWritter.write(session.getLecturer() + ",");
		bufferWritter.write(session.getMaxAttendance() + ",");
		bufferWritter.write(session.getRepeatFrequency() + ",");
		bufferWritter.write(session.isCompulsory() + ",");
		bufferWritter.write(session.getTime() + ",");
		bufferWritter.write(session.getVenue() + ",");
		bufferWritter.write(session.getCourseName() + ",");
		bufferWritter.write("\n");
		bufferWritter.close();

		return "sessionresult";
	}

}
