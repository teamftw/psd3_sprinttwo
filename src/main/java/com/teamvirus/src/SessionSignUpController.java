package com.teamvirus.src;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.teamvirus.src.Session;

@Controller
public class SessionSignUpController {

	@RequestMapping(value = "/studentsessionsignup")
	private ModelAndView selectTag() throws FileNotFoundException {
		ModelAndView mav = new ModelAndView("studentsessionsignup");

		String directory = "s1sessionlist.csv";
		
		boolean isFound;
		
		isFound = new File(directory).exists();
		if (isFound == false) {
			//SessionNotCreated();
			return new ModelAndView("FileNotFoundError");
		}

		FileReader reader1 = new FileReader(directory);
		Scanner scanner1 = new Scanner(reader1);

		ArrayList<String> sessionList = new ArrayList<String>();

		 String tempString;
		 while (scanner1.hasNextLine()) {
		 tempString = scanner1.nextLine();
		 if (tempString.equals("")) {
		 break;
		 }
		 sessionList.add(tempString);
		 }

		scanner1.close();

		Map<String, String> tempMap = new HashMap<String, String>();

		for (int i = 0; i < sessionList.size(); i++) {
			tempMap.put(sessionList.get(i), sessionList.get(i));
		}

		mav.addObject("dropdownlistMap", tempMap);
		mav.addObject("s1", new Session());

		return mav;
	}
	
	public String SessionNotCreated() throws FileNotFoundException{
		return"FileNotFoundError";
	}

	@RequestMapping(value = "/studentsessionsignupresult")
	private ModelAndView processPhone(@ModelAttribute Session s1)
			throws IOException {
		ModelAndView mav = new ModelAndView("studentsessionsignupresult");
		mav.addObject("s1", s1);

		File file = new File("student1timetable.csv");

		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		// true = append file
		FileWriter fileWritter = new FileWriter(file, true);
		BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
		bufferWritter.write(s1.getSessionS());
		bufferWritter.write("\n");
		bufferWritter.close();

		return mav;
	}

}