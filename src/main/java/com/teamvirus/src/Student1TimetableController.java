package com.teamvirus.src;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Student1TimetableController {
	@RequestMapping("/student1timetable")
	public String student1timetable(Model model) throws FileNotFoundException {

		String directory = "student1timetable.csv";

		boolean isFound;
		isFound = new File(directory).exists();
		if (isFound == false) {
			return "FileNotFoundError";
		}

		FileReader reader = new FileReader(directory);
		Scanner scanner1 = new Scanner(reader);

		ArrayList<String> sessionList = new ArrayList<String>();

		scanner1.useDelimiter(",");

		while (scanner1.hasNext()) {
			sessionList.add(scanner1.next());
		}

		scanner1.close();

		Date dt = new Date();
		String currentDate = dt.toString();
		model.addAttribute("currentDate", currentDate);

		Calendar now = Calendar.getInstance();
		int tyear = now.get(Calendar.YEAR);
		int tmonth = now.get(Calendar.MONTH);
		tmonth++;
		int tday = now.get(Calendar.DAY_OF_MONTH);

		String day = String.valueOf(tday);
		if (day.length() == 1) {
			day = "0" + day;
		}

		String month = String.valueOf(tmonth);
		if (month.length() == 1) {
			month = "0" + month;
		}

		String year = String.valueOf(tyear);

		int i = 8;
		int counter = 1;

		for (i = 8; i < sessionList.size(); i += 9) {
			String tempstr1 = sessionList.get(i - 8);

			if (tempstr1.substring(0, 1).equals("\n")) {
				tempstr1 = tempstr1.substring(1, tempstr1.length());
			}

			int tempDay = Integer.parseInt(tempstr1.substring(0, 2));

			boolean isTrue = false;
			if (tempDay >= tday - 7 || tempDay <= tday + 7) {
				isTrue = true;
			}

			if (isTrue == true
					&& sessionList
							.get(i - 8)
							.substring(sessionList.get(i - 8).length() - 7,
									sessionList.get(i - 8).length() - 5)
							.equals(month)
					&& sessionList
							.get(i - 8)
							.substring(sessionList.get(i - 8).length() - 4,
									sessionList.get(i - 8).length())
							.equals(year)) {
				model.addAttribute("class" + counter, sessionList.get(i));
				model.addAttribute("classtime" + counter,
						sessionList.get(i - 2));
				model.addAttribute("classlocation" + counter,
						sessionList.get(i - 1));
				model.addAttribute("classdate" + counter,
						sessionList.get(i - 8));
				counter++;
			}
		}

		return "student1timetable";
	}

	@RequestMapping("/student1yearlytimetable")
	public String student1yearlytimetable(Model model)
			throws FileNotFoundException {

		String directory = "student1timetable.csv";

		boolean isFound;
		isFound = new File(directory).exists();
		if (isFound == false) {
			return "FileNotFoundError";
		}

		FileReader reader = new FileReader(directory);
		Scanner scanner1 = new Scanner(reader);

		ArrayList<String> sessionList = new ArrayList<String>();

		scanner1.useDelimiter(",");

		while (scanner1.hasNext()) {
			sessionList.add(scanner1.next());
		}

		scanner1.close();

		Calendar now = Calendar.getInstance();
		int tyear = now.get(Calendar.YEAR);
		int tmonth = now.get(Calendar.MONTH);
		tmonth++;
		int tday = now.get(Calendar.DAY_OF_MONTH);

		String day = String.valueOf(tday);
		if (day.length() == 1) {
			day = "0" + day;
		}

		String month = String.valueOf(tmonth);
		if (month.length() == 1) {
			month = "0" + month;
		}

		String year = String.valueOf(tyear);

		model.addAttribute("currentYear", year);

		int i = 8;
		int counter = 1;
		for (i = 8; i < sessionList.size(); i += 9) {

			if (sessionList
					.get(i - 8)
					.substring(sessionList.get(i - 8).length() - 4,
							sessionList.get(i - 8).length()).equals(year)) {
				model.addAttribute("class" + counter, sessionList.get(i));
				model.addAttribute("classtime" + counter,
						sessionList.get(i - 2));
				model.addAttribute("classlocation" + counter,
						sessionList.get(i - 1));
				model.addAttribute("classdate" + counter,
						sessionList.get(i - 8));
				counter++;
			}
		}

		return "student1yearlytimetable";
	}

	@RequestMapping("/student1dailytimetable")
	public String student1dailytimetable(Model model)
			throws FileNotFoundException {

		String directory = "student1timetable.csv";
		
		boolean isFound;
        isFound = new File(directory).exists();
        if (isFound == false) {
        	return "FileNotFoundError";
        }

		FileReader reader = new FileReader(directory);
		Scanner scanner1 = new Scanner(reader);

		ArrayList<String> sessionList = new ArrayList<String>();

		scanner1.useDelimiter(",");

		while (scanner1.hasNext()) {
			sessionList.add(scanner1.next());
		}

		scanner1.close();

		Date dt = new Date();
		String currentDate = dt.toString();

		Calendar now = Calendar.getInstance();
		int tyear = now.get(Calendar.YEAR);
		int tmonth = now.get(Calendar.MONTH);
		tmonth++;
		int tday = now.get(Calendar.DAY_OF_MONTH);

		String day = String.valueOf(tday);
		if (day.length() == 1) {
			day = "0" + day;
		}

		String month = String.valueOf(tmonth);
		if (month.length() == 1) {
			month = "0" + month;
		}

		String year = String.valueOf(tyear);

		model.addAttribute("currentDate", currentDate);

		int i = 8;
		int counter = 1;
		for (i = 8; i < sessionList.size(); i += 9) {
			if (sessionList
					.get(i - 8)
					.substring(sessionList.get(i - 8).length() - 10,
							sessionList.get(i - 8).length() - 8).equals(day)
					&& sessionList
							.get(i - 8)
							.substring(sessionList.get(i - 8).length() - 7,
									sessionList.get(i - 8).length() - 5)
							.equals(month)
					&& sessionList
							.get(i - 8)
							.substring(sessionList.get(i - 8).length() - 4,
									sessionList.get(i - 8).length())
							.equals(year)) {
				model.addAttribute("class" + counter, sessionList.get(i));
				model.addAttribute("classtime" + counter,
						sessionList.get(i - 2));
				model.addAttribute("classlocation" + counter,
						sessionList.get(i - 1));
				model.addAttribute("classdate" + counter,
						sessionList.get(i - 8));
				counter++;
			}
		}

		return "student1dailytimetable";
	}
}
