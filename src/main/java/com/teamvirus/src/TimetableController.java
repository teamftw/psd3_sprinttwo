package com.teamvirus.src;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TimetableController {
	@RequestMapping("/timetable")
	public String timetable(Model model) throws FileNotFoundException {

		String directory = "s1sessionlist.csv";
		
		boolean isFound;
        isFound = new File(directory).exists();
        if (isFound == false) {
        	return "AdminFileNotFoundError";
        }

		FileReader reader = new FileReader(directory);
		Scanner scanner1 = new Scanner(reader);

		ArrayList<String> sessionList = new ArrayList<String>();

		scanner1.useDelimiter(",");

		while (scanner1.hasNext()) {
			sessionList.add(scanner1.next());
		}

		scanner1.close();

		model.addAttribute("class1", sessionList.get(8));
		model.addAttribute("classtime1", sessionList.get(6));
		model.addAttribute("classlocation1", sessionList.get(7));
		
		int i = 8;
		int counter = 1;
		for (i = 8; i < sessionList.size(); i += 9) {
				model.addAttribute("class" + counter, sessionList.get(i));
				model.addAttribute("classtime" + counter,
						sessionList.get(i - 2));
				model.addAttribute("classlocation" + counter,
						sessionList.get(i - 1));
				counter++;
			}
		
		return "timetable";
	}
}
