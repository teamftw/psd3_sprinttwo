package com.teamvirus.src;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.teamvirus.src.Session;

@Controller
public class sessionSign {

	@RequestMapping(value = "/test")
	private ModelAndView selectTag() throws FileNotFoundException {
		ModelAndView mav = new ModelAndView("studentSignUp");

		String directory = "test.csv";
		
		FileReader reader = new FileReader(directory);
		Scanner scanner1 = new Scanner(reader);
		
		ArrayList<String> sessionList = new ArrayList<String>();
				
		while (scanner1.hasNext()){
			sessionList.add(scanner1.next());
		}

		scanner1.close();
		
		Map<String, String> phones = new HashMap<String, String>();
		
		for (int i = 0; i < sessionList.size(); i++){
			phones.put(i +" Session :", sessionList.get(i));
			
		}

		mav.addObject("phonesMap", phones);
		mav.addObject("smartphone", new Session());

		return mav;
	}
	
	@RequestMapping(value="/studentSignUpResult")
    private ModelAndView processPhone(@ModelAttribute Session smartphone) {
            ModelAndView mav = new ModelAndView("studentSignUpResult");
            mav.addObject("smartphone", smartphone);                
            return mav;
    }
	
	


}