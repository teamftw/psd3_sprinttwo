<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>No session found.</title>
</head>
<body>
	<p>Please create a session before viewing available sessions.</p> 
	<a href="/src/dashboard?username=admin">Return to DashBoard</a>
	<br>
	<a href="/src">Logout</a>
</body>
</html>