<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>No session found.</title>
</head>
<body>
	<p>Please sign up for a session before viewing the timetable.</p> 
	<p>The timetable is currently empty.</p>
	<a href="/src/dashboard?username=student1">Return to Dashboard</a>
	<br>
	<a href="/src">Logout</a>
</body>
</html>