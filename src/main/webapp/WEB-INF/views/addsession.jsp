<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE HTML>
<html>
<head>
<title>UMS: Add Session</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
	You are in: Dashboard > Add Session

	<h1>Session Form</h1>
	<form:form method="post" commandname="session" action="addsession">
		<table>
			<tr>
				<td width="127"><form:label path="date">Date (dd/mm/yyyy)</form:label></td>
				<td width="102"><form:input path="date" /></td>
			</tr>
			<tr>
				<td width="127"><form:label path="time">Time (hh:mm)</form:label></td>
				<td width="102"><form:input path="time" /></td>
			</tr>
			<tr>
				<td width="127"><form:label path="courseName">Course Name</form:label></td>
				<td width="102"><form:input path="courseName" /></td>
			</tr>
			<tr>
				<td width="127"><form:label path="duration">Duration</form:label></td>
				<td width="102"><form:input path="duration" /></td>
			</tr>
			<tr>
				<td width="127"><form:label path="repeatFrequency">Frequency</form:label></td>
				<td width="102"><form:input path="repeatFrequency" /></td>
			</tr>
			<tr>
				<td width="127"><form:label path="lecturer">Lecturer</form:label></td>
				<td width="102"><form:input path="lecturer" /></td>
			</tr>
			<tr>
				<td width="127"><form:label path="maxAttendance">Max. Attendance</form:label></td>
				<td width="102"><form:input path="maxAttendance" /></td>
			</tr>
			<tr>
				<td width="127"><p>Compulsory?</p></td>
				<td width="127"><form:checkbox path="compulsory"></form:checkbox></td>
			</tr>
			<tr>
				<td width="127"><form:label path="venue">Venue</form:label></td>
				<td width="102"><form:input path="venue" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
	<a href="/src/dashboard?username=admin">Return to
		Dashboard</a>
	<br>
	<a href="/src">Logout</a>
</body>
</html>
