<!--
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
-->
<%@ page session="false"%>
<html>
<head>
<title>UMS: Login</title>
</head>
<body>
	<h1>Login</h1>

	<P>
	<form method="get" action="authenticate">
		<p>
			<label for="username">Username </label><input type="text"
				name="username" />
		</p>
		<p>
			<label for="password">Password </label><input type="password"
				name="password" />
		</p>
		<p>
			<input type="submit" value="Log in" />
		</p>
	</form>
</body>
</html>
