<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE HTML>
<html>
<head>
<title>UMS: Session Results</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
	<p>You are in: Dashboard > Add Session > Session Result</p>

	<h1>Session submitted.</h1>
	<table>
		<tr>
			<td width="127">Date:</td>
			<td width="127">${date }</td>
		</tr>
		<tr>
			<td width="127">Time:</td>
			<td width="127">${time }</td>
		</tr>

		<tr>
			<td width="127">Duration:</td>
			<td width="127">${duration }</td>
		</tr>
		<tr>
			<td width="127">Frequency:</td>
			<td width="127">${repeatFrequency }</td>
		</tr>
		<tr>
			<td width="127">Lecturer</td>
			<td width="127">${lecturer }</td>
		</tr>
		<tr>
			<td width="127">Max attendance</td>
			<td width="127">${MaxAttendance }</td>
		</tr>
		<tr>
			<td width="127">Compulsory:</td>
			<td width="127">${isCompulsory}</td>
		</tr>
		<tr>
			<td width="127">Venue</td>
			<td width="127">${venue }</td>
		</tr>

	</table>

	<a href="addsession">Add another session</a>
	<br>
	<a href="timetable">View all available sessions</a>
	<br>
	<a href="/src/dashboard?username=admin">Return to DashBoard</a>
	<br>
	<a href="/src">Logout</a>

</body>
</html>
