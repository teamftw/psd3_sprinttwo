<!DOCTYPE HTML>
<html xmlns:th="http://www.thymeleaf.org">
<head>
<title>UMS: Timetable</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
	<p>You are in: Dashboard > Yearly Timetable</p>
	<p>Displaying timetable for year: ${currentYear}.</p>
	<table border="1" bordercolor="#FFFFFF"
		style="background-color: #FFFFCC" width="30%" cellpadding="3"
		cellspacing="1">
		<tr>
			<td><b>Date</b></td>
			<td><b>Course Name</b></td>
			<td><b>Time</b></td>
			<td><b>Location</b></td>
		</tr>
		<tr>
			<td>${classdate1}</td>
			<td>${class1}</td>
			<td>${classtime1}</td>
			<td>${classlocation1}</td>
		</tr>
		<tr>
			<td>${classdate2}</td>
			<td>${class2}</td>
			<td>${classtime2}</td>
			<td>${classlocation2}</td>
		</tr>
		<tr>
			<td>${classdate3}</td>
			<td>${class3}</td>
			<td>${classtime3}</td>
			<td>${classlocation3}</td>
		</tr>
		<tr>
			<td>${classdate4}</td>
			<td>${class4}</td>
			<td>${classtime4}</td>
			<td>${classlocation4}</td>
		</tr>
		<tr>
			<td>${classdate5}</td>
			<td>${class5}</td>
			<td>${classtime5}</td>
			<td>${classlocation5}</td>
		</tr>
		<tr>
			<td>${classdate6}</td>
			<td>${class6}</td>
			<td>${classtime6}</td>
			<td>${classlocation6}</td>
		</tr>
		<tr>
			<td>${classdate7}</td>
			<td>${class7}</td>
			<td>${classtime7}</td>
			<td>${classlocation7}</td>
		</tr>
		<tr>
			<td>${classdate8}</td>
			<td>${class8}</td>
			<td>${classtime8}</td>
			<td>${classlocation8}</td>
		</tr>
		<tr>
			<td>${classdate9}</td>
			<td>${class9}</td>
			<td>${classtime9}</td>
			<td>${classlocation9}</td>
		</tr>
		<tr>
			<td>${classdate10}</td>
			<td>${class10}</td>
			<td>${classtime10}</td>
			<td>${classlocation10}</td>
		</tr>
	</table>

	<a href="/src/dashboard?username=student1">Return to Dashboard</a>
	<br>
	<a href="/src">Logout</a>

</body>
</html>
