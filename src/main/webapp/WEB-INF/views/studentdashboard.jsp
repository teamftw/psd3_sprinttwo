<html>
<head>
<title>UMS: Student Dashboard</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
	<p>You are in: Dashboard</p>

	<h2>Hello ${username}</h2>
	<ul>
		<li><a href="studentsessionsignup">Sign up for Course</a></li>
		<li><a href="student1dailytimetable">View my Daily timetable</a></li>
		<li><a href="student1timetable">View my Weekly timetable</a></li>
		<li><a href="student1yearlytimetable">View my Yearly timetable</a></li>
	</ul>
	
	<a href="/src">Logout.</a>
</body>
</html>