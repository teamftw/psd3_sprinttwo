<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>UMS: Student Sign up</title>
</head>
<body>
	<h1>Session Register page</h1>
	Selected Session:
	<br /> ${s1.sessionS}
	<br />
	<p> Session has been registered.</p>
	
	<a href="student1timetable">View new timetable</a>
	<br>
	<a href="javascript:history.go(-2)">Return to Dashboard</a>
	<br>
	<a href="/src">Logout.</a>
</body>
</html>