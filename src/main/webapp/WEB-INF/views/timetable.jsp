<!DOCTYPE HTML>
<html xmlns:th="http://www.thymeleaf.org">
<head>
<title>UMS: Timetable</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
	<p>You are in: Dashboard > All Available Sessions</p>

	<table border="1" bordercolor="#FFFFFF"
		style="background-color: #FFFFCC" width="30%" cellpadding="3"
		cellspacing="1">
		<tr>
			<td> ${class1}</td>
			<td> ${classtime1}</td>
			<td> ${classlocation1 }</td>
		</tr>
		<tr>
			<td> ${class2}</td>
			<td> ${classtime2 }</td>
			<td> ${classlocation2 }</td>
		</tr>
		<tr>
			<td> ${class3}</td>
			<td> ${classtime3 }</td>
			<td> ${classlocation3 }</td>
		</tr>
		<tr>
			<td> ${class4}</td>
			<td> ${classtime4 }</td>
			<td> ${classlocation4 }</td>
		</tr>
		<tr>
			<td> ${class5}</td>
			<td> ${classtime5 }</td>
			<td> ${classlocation5 }</td>
		</tr>
	</table>

	<a href="/src/dashboard?username=admin">Return to Dashboard.</a>
	<br>
	<a href="/src">Logout</a>

</body>
</html>
